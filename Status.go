package assignment2

import (
	"encoding/json"
	"log"
	"net/http"
	"time"
)

var upTime = time.Now() // this takes a timestamp at the start of the program

func StatusHandler(w http.ResponseWriter, r *http.Request) {

	// I know https://git.gvk.idi.ntnu.no/api/v4/projects is always up, so if I get the data, it is working
	gitResp, err := http.Get("https://git.gvk.idi.ntnu.no/api/v4/projects")
	if err != nil {
		log.Fatalln(err)
	}

	// stolen from discord
	dbStatus := 200 //Assumes it to be ok
	//Doc("0") is reserved for status checks
	_, err = Db.Client.Collection("webhooks").Doc("0").Get(Db.Ctx)
	if err != nil {
		//Can not get to server for unknown reason
		//Gives a Service Unavailable error
		dbStatus = 503
	}

	// I save them away to give to the diagData struct
	Statusgit := gitResp.StatusCode

	// I take a new timestamp at this point and find the difference between now and the old time
	newTime := time.Now()
	bigTime := int64(newTime.Sub(upTime) / time.Second)

	statusData := Status{Statusgit, dbStatus, "v1", bigTime}

	// This prints the json struct to the website
	w.Header().Add("content-type", "application/json")
	_ = json.NewEncoder(w).Encode(statusData)

}
