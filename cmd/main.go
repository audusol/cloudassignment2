package main

import (
	"cloudassignment2"
	"fmt"
	"html"
	"log"
	"net/http"
)

func defaultHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, this is the default page\n"+
		"use /repocheck/v1/commits\n"+
		"/repocheck/v1/languages\n"+
		"/repocheck/v1/webhooks\n"+
		"or /repocheck/v1/status instead", html.EscapeString(r.URL.Path))
}

func main() {

	const projectID = "cloudassignment2-fed1b"
	const collection = "webhooks"

	assignment2.Db = assignment2.FirestoreDatabase{ProjectID: projectID, CollectionName: collection}
	err := assignment2.Db.Init()
	if err != nil {
		log.Fatal(err)
	}

	defer assignment2.Db.Close()

	http.HandleFunc("/", defaultHandler)
	http.HandleFunc("/repocheck/v1/commits", assignment2.CommitsHandler)
	http.HandleFunc("/repocheck/v1/languages/", assignment2.LanguagesHandler)
	http.HandleFunc("/repocheck/v1/issues", assignment2.IssuesHandler)
	http.HandleFunc("/repocheck/v1/webhooks", assignment2.WebHooksHandler)
	http.HandleFunc("/repocheck/v1/webhooks/", assignment2.WebHooksHandler)
	http.HandleFunc("/repocheck/v1/status/", assignment2.StatusHandler)

	println("listening on ", 8080)

	log.Fatal(http.ListenAndServe(":8080", nil))
}
