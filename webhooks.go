package assignment2

import (
	"cloud.google.com/go/firestore"
	"encoding/json"
	"fmt"
	"google.golang.org/api/iterator"
	"log"
	"net/http"
)

var Db FirestoreDatabase

func WebHooksHandler(w http.ResponseWriter, r *http.Request) {

	switch r.Method {

	case http.MethodGet:

		webHookId := ""
		webHookId = r.URL.Path[23:]
		var iteration *firestore.DocumentIterator
		var webhook Webhook
		var listWebhook []Webhook
		// makes variables

		if webHookId != "" { // if the id is empty, put only the id in the iteration
			iteration = Db.Client.Collection("webhooks").Where("Id", "==", webHookId).Documents(Db.Ctx)
		} else { // else put all
			iteration = Db.Client.Collection("webhooks").Documents(Db.Ctx)
		}

		for {
			doc, err := iteration.Next() // loops through the document
			if err == iterator.Done {    // if the document is finished
				break // kill it
			}
			if err != nil { // if the iterator fails
				log.Fatalf("Failed to iterate: %v", err) // kill it
			}
			if doc != nil { // and if all is good
				err = doc.DataTo(&webhook)                 // fix it
				listWebhook = append(listWebhook, webhook) // add it to the array
			}
		}

		if len(listWebhook) != 0 {
			// makes header and encodes, the normal stuff
			w.Header().Add("content-type", "application/json")
			err := json.NewEncoder(w).Encode(listWebhook)
			if err != nil { // a friend of mine told me to have a big error code, so yeah
				http.Error(w, "failed to encode webhoks", http.StatusInternalServerError)
				fmt.Println("failed to encode")
			}
		}else {
			fmt.Fprintf(w, "faulty Id")
		}

	case http.MethodDelete:
		// deletes
		var deleteHook Webhook

		webhookIdentifier := r.URL.Path[23:] // finds the Id
		deleteHook.Id = webhookIdentifier    // puts it where it's supposed to be

		err := Db.Delete(&deleteHook) // do the delete function
		if err != nil {
			log.Fatal(err) // all is good, or else!
		}

	case http.MethodPost:
		// post
		var postHook Webhook

		// decode from the body
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&postHook)
		if err != nil {
			fmt.Println("Failed to decode json from user in post request")
		}

		err = Db.Save(&postHook) // do the save function
		if err != nil {
			log.Fatal(err)
		}

	default:
		// default function
		fmt.Fprintf(w, "Unknown REST method %v", r.Method)
	}
}
