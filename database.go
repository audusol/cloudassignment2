package assignment2

import (
	"cloud.google.com/go/firestore"
	"context"
	"fmt"
	"google.golang.org/api/option"
	"log"
)

// these are stolen from Mariusz' repo, so I don't really know the details about the specifics of their use.
func (db *FirestoreDatabase) Init() error {
	db.Ctx = context.Background()
	var err error
	sa := option.WithCredentialsFile("./cloudassignment2-fed1b-firebase-adminsdk-y5tr0-28e4befe7b.json")
	db.Client, err = firestore.NewClient(db.Ctx, db.ProjectID, sa)
	if err != nil {
		fmt.Printf("Error in FirebaseDatabase.Init() function: %v\n", err)
		log.Fatal(err, "Error in FirebaseDatabase.Save()")
	}
	return nil
}

func (db *FirestoreDatabase) Close() {
	_ = db.Client.Close()
}

func (db *FirestoreDatabase) Save(s *Webhook) error {
	ref := db.Client.Collection(db.CollectionName).NewDoc()
	s.Id = ref.ID
	_, err := ref.Set(db.Ctx, s)
	if err != nil {
		fmt.Println("ERROR saving student to Firestore DB: ", err)
		log.Fatal(err, "Error in FirebaseDatabase.Save()")
	}
	return nil
}

func (db *FirestoreDatabase) Delete(s *Webhook) error {
	docRef := db.Client.Collection(db.CollectionName).Doc(s.Id)
	_, err := docRef.Delete(db.Ctx)
	if err != nil {
		fmt.Printf("ERROR deleting student (%v) from Firestore DB: %v\n", s, err)
		log.Fatal(err, "Error in FirebaseDatabase.Save()")
	}
	return nil
}
