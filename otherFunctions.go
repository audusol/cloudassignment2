package assignment2

import (
	"bytes"
	"encoding/json"
	"fmt"
	"google.golang.org/api/iterator"
	"log"
	"net/http"
	"strconv"
	"time"
)

func authFinder(r *http.Request) string {
	if r.URL.Query()["private_token"] != nil { // see if token is in the URL
		auth := r.URL.Query()["private_token"][0]
		return auth // returns the token if it is good
	}
	return "" // returns blank if not
}

func limitFinder(r *http.Request) int {
	if r.URL.Query()["limit"] != nil { // see if limit is in the URL
		customLimit := r.URL.Query()["limit"][0]         // finds the string of the "number"
		customLimitInt, err := strconv.Atoi(customLimit) // converts from string to int
		if err == nil {
			return customLimitInt // if all goes well, we now have a good limit
		} else {
			log.Fatalln(err)
		}
	} //and if the user doesn't write a limit, we use limit = 5, with the user noticing no difference
	return 5
}

func pageFinder(auth string) int {
	page := 1
	resp, err := http.Get(fmt.Sprintf("https://git.gvk.idi.ntnu.no/api/v4/projects?per_page=100&private_token=%s", auth))
	if err != nil {
		log.Fatalln(err)
	}
	if resp.StatusCode == 200 { // does a look up of the page with the authorized token, if the site is up
		page, err = strconv.Atoi(resp.Header.Get("X-Total-Pages")) // finds amount of pages
		if err != nil {
			log.Fatalln(err)
		}
	}
	return page // and returns them
}

func statusFinder(auth string) int {
	// quick get to see if the website is up and the token is valid
	resp, err := http.Get(fmt.Sprintf("https://git.gvk.idi.ntnu.no/api/v4/projects?per_page=100&private_token=%s", auth))
	if err != nil {
		log.Fatalln(err)
	}

	return resp.StatusCode
}

func webHooksInvoke(event string, params []string, currentTime time.Time) {
	iter := Db.Client.Collection("webhooks").Documents(Db.Ctx)
	for { // goes through all webhooks in the database
		var webHook Webhook
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Printf("Failed to iterate: %v", err)
		}
		err = doc.DataTo(&webHook)
		if err != nil {
			log.Printf("Failed to parse database response: %v", err)
		}
		if webHook.Event == event { // if the event in the database is the same as the event sent with
			requestWebhook := Invocation{Event: event, Params: params, Time: time.Now()}
			requestBody, err := json.Marshal(requestWebhook)
			if err != nil {
				log.Printf("Failed to marshall webhook call: %v", err)
			}
			_, err = http.Post(webHook.Url, "application/json", bytes.NewBuffer(requestBody)) // send post
			if err != nil {
				log.Printf("Not able to send webhook call: %v", err)
			}
		}
	}
}
